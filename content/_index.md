## Règlement intérieur de la médiathèque
- [Français](reglement/reglement.pdf)  ([signer le réglement](https://sondages.inria.fr/index.php/242818?lang=fr))
- [English](reglement/regulations.pdf) ([sign the regulations](https://sondages.inria.fr/index.php/242818?lang=en))

## Catalogue
- [Catalogue des BDs triés par genre et par série](comics/)
    - [seulement les complètes](comics/complete/)
- [Catalogue des jeux](./catalogue.pdf)
- [Pour voir ce qui est disponible ou emprunté (VPN ou sur site)](https://mediatheque-agos.bordeaux.inria.fr/)

## Les permanences
- [le calendrier des permanences](https://zimbra.inria.fr/home/bordeaux-mediatheque-agos@zimbra-local.inria.fr/Calendar.html)
- S'il n'y en a pas, demandez !
- Pour déposer une perm, créez vous un rendez-vous dans zimbra en ajoutant la salle "bordeaux-mediatheque-agos".

## Nous contacter
- [Mattermost](https://mattermost.inria.fr/bso/channels/mediatheque)
